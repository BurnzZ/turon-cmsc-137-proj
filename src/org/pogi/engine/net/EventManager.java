package org.pogi.engine.net;

import java.util.HashMap;
import java.util.Map;

import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.engine.net.javanet.Server;

/**
 * 
 * This is responsible for managing the events 
 * contained in Messages. This should be extended (and implemented)
 * for both client and the server side. ie. Client
 * and server handle client-side events and server-side events,
 * respectively.
 * 
 * Therefore, both {@link Client#setEventManager(EventManager)} and {@link Server#setEventManager(EventManager)} 
 * should be invoked in order to handle events from messages
 *
 */
public abstract class EventManager {
	Map<EventType, IEvent> eventMap = new HashMap<EventType, IEvent>(); //String messageType key, Event corresponding event
	
	public EventManager(){
		defineEvents();
	}
	
	/**
	 * This should be implemented and contain definitions for Event Handlers
	 * by calling a number of {@link #putEventHandler(EventType, IEvent)}
	 */
	public abstract void defineEvents();
	
	/**
	 * handle a Message received from Client or Server
	 * @param message a Message containing event information
	 */
	public void handle(Message message){
		//System.out.println("("+this.getClass()+") LOG: handling event from message");
		EventType type = (EventType) message.get("type");
		IEvent event = eventMap.get(type);
		
		if(event != null)
			event.apply(message);
		else {
			//System.out.println("("+this.getClass()+") LOG: Unmapped EventType: "+type+". Event not applied.");
		}
	}
	
	/**
	 * put an Event handler defined by the user into the event map
	 */
	public void putEventHandler(EventType messageType, IEvent event){
		eventMap.put(messageType, event);
	}
}

