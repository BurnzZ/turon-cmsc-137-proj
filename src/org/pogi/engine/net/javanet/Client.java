package org.pogi.engine.net.javanet;

import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Random;
import java.util.logging.Logger;

import org.pogi.engine.net.EventManager;
import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.constant.NetworkConstants;

/**
 *  Message passing java.net implementation for the client-side.
 *  responsible for message passing between client and server
 * 
 *	TODO : {@link Client} and {@link Server} has a number of same methods. 
 *  They should share a single interface, Maybe INetwork or something
 */
public class Client {
	private static Object gameManager;
	private static String username;  //id is a better name for this
	private static String serverAddress;
	private static final Logger logger = Logger.getLogger( Client.class.getName() );
	private static EventManager eventManager;
	private static DatagramSocket socket;	
	private static boolean connected;
	
	public static void joinHost(String hostname){
		try{
			serverAddress = hostname;
			
			InetAddress groupAddress = InetAddress.getByName(NetworkConstants.GROUP_ADDRESS); //TODO address pooling
			
			Random rand = new Random();
			
		    int randomPort = rand.nextInt((NetworkConstants.MAX_PORT - NetworkConstants.MIN_PORT) + 1) + NetworkConstants.MIN_PORT;
			socket = new DatagramSocket(randomPort);
			/*
				socket.setInterface(Inet4Address.getLocalHost());
				socket.joinGroup(groupAddress);
			*/
			
			//process incoming messages from the server
			new PacketReceiver().start();
			connected  = true;
			//System.out.println("(Client) Packet receiver thread started");
			
			Message handshakeMessage = new Message(EventType.HANDSHAKE);
			handshakeMessage.put("username", username);
			sendMessage(handshakeMessage, hostname);
			
		}catch(IOException e){
			e.printStackTrace();
			serverAddress = null;
		}
		
	}
	
	public static void disconnect(){
		connected = false;
		socket.close();
	}
	
	public static void setEventManager(EventManager em){
		eventManager = em;
	}
	
	public static void onMessageReceived(Message message) {
		eventManager.handle(message);
	}

	public static void sendMessage(Message message, String hostname){
		try{
			//System.out.println("(Client) Sending a "+ message.get("type") +" message...");

			String stringToSend = message.toString();
			DatagramPacket packet = new DatagramPacket(
					stringToSend.getBytes(), 
					stringToSend.length(), 
					InetAddress.getByName(hostname), 
					NetworkConstants.SERVER_PORT);
			socket.send(packet);
			
			//System.out.println("(Client) LOG: Packet sent to address :" + hostname);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//Asynchronous Packet Handling
	public static class PacketReceiver extends Thread{

		public void run() {
			while(connected){
				byte[] buf = new byte[8192];
				
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				//System.out.println(" (Client) waiting to receive packet..." );
				try { 
					socket.receive(packet); 
					
					//System.out.println(" (Client) packet received. extracting message..." );
					String receivedPacketString = new String(packet.getData(), packet.getOffset(), packet.getLength());
					Message receivedMessage = new Message(receivedPacketString);

					//System.out.println("(Client) LOG: Message extracted. message : \""+receivedMessage+"\" handling message...");
					
					onMessageReceived(receivedMessage);
					
					//System.out.println("(Client) LOG: Message handled. DONE");

				} catch (IOException e) {
					break;
				}
			}
		}
	}
	
	public static void setGameManager(Object m){
		gameManager = m;
	}
	
	public static Object getGameManager(){
		return gameManager;
	}

	public static EventManager getMessageHandler() {
		return eventManager;
	}
	
	public static void setUsername(String uname){
		username = uname;
	}
	
	public static String getUsername(){
		return username;
	}
	
	public static String getServerAddress(){
		return serverAddress;
	}
	
	public static void setServerAddres(String sa){
		serverAddress = sa;
	}
	

}
