package org.pogi.engine.net.javanet;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.pogi.engine.net.EventManager;
import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.constant.NetworkConstants;

/**
 *	Message passing java.net implementation for the server-side.
 *  Responsible for message passing between client and server
 * 
 *	TODO : {@link Client} and {@link Server} has a number of same methods. 
 *  They should share a single interface, Maybe INetwork or something
 */
public class Server {
	private static Object gameManager;
	private static EventManager eventManager;

	static Map<String, InetSocketAddress> clients;
	static String address;
	
	public boolean endOfTheWorld = false;
	public static DatagramSocket socket = null;
	
	public static void run(){
		try {
			clients = new HashMap<String, InetSocketAddress>();

			socket = new DatagramSocket(NetworkConstants.SERVER_PORT, InetAddress.getLocalHost());
			
			address = socket.getLocalAddress().getHostAddress(); //TODO baguhin mo to allan
			//System.out.println("(Server) LOG: Initialized Chat Server Socket to Port 4445 with host address: "+address);
			(new PacketReceiver()).start();
			//System.out.println("(Server) LOG: Packet receiver thread started.");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void disconnectClient(String username){
		clients.remove(username);
	}
	
	//These three methods should be interface methods as Client also has these
	public static void setEventManager(EventManager em){
		eventManager = em;
	}
	
	public static void onMessageReceived(Message message) {
		eventManager.handle(message);
	}

	public static void sendMessage(Message message, String username) {
		try{
			System.out.println("(Server) Sending a "+ message.get("type") +" message...");
			
			InetSocketAddress clientSocketAddress = clients.get(username);
			
			String stringToSend = message.toString();
			DatagramPacket packet = new DatagramPacket(
					stringToSend.getBytes(),
					stringToSend.length(), 
					clientSocketAddress.getAddress(),
					clientSocketAddress.getPort());
			socket.send(packet);
			
			//System.out.println("(Server) LOG: Packet sent to address :" + clientSocketAddress.getAddress());
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void broadcastMessage(Message message) {
		try{
			/*
				System.out.println("(Server) LOG: Broadcasting message of type :" + message.get("type"));
				
				String stringToSend = message.toString();
				DatagramPacket packet = new DatagramPacket(
						stringToSend.getBytes(), 
						stringToSend.length(), 
						InetAddress.getByName(groupAddress), 
						NetworkConstants.GROUP_CLIENT_PORT);
				socket.send(packet);
				
				System.out.println("(Server) LOG: Packet sent to group :" + groupAddress);
			
			System.out.println("(Server) LOG: Packet sent to group :" + groupAddress);
			*/
			if(!((EventType) message.get("type")).equals(EventType.GAME_UPDATE)){
				//System.out.println("(Server) LOG: Broadcasting message of type :" + message.get("type"));
			}
			
			String stringToSend = message.toString();
			DatagramPacket packet = new DatagramPacket(
					stringToSend.getBytes(), 
					stringToSend.length(), 
					null, 
					0);
			
			for(Map.Entry<String, InetSocketAddress> entry : clients.entrySet()){
				packet.setAddress(entry.getValue().getAddress());
				packet.setPort(entry.getValue().getPort());
				socket.send(packet);
			}
			
			//System.out.println("(Server) LOG: Packet sent to group :" + groupAddress);
		
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	public static void setGameManager(Object m){
		gameManager = m;
	}
	
	public static Object getGameManager(){
		return gameManager;
	}
	
	public static void putClient(String username, InetSocketAddress  address){
		clients.put(username, address);
	}
	
	public static InetSocketAddress getClientAddress(String username){
		return clients.get(username);
	}

	public static String getAddress() {
		return address;
	}
	
	public static class PacketReceiver extends Thread{
		public void run() {
			while(true){
				try {
					byte[] buf = new byte[8192];

					DatagramPacket receivedPacket = new DatagramPacket(buf, buf.length);
					//System.out.println("(Server) LOG: Waiting to receive a packet...");
					socket.receive(receivedPacket);
					//System.out.println("(Server) LOG: Packet received. extracting message...");
					String receivedPacketString = 
							new String(receivedPacket.getData(), 
									receivedPacket.getOffset(), 
									receivedPacket.getLength());
					
					Message receivedMessage = new Message(receivedPacketString);
					
					InetSocketAddress clientSocketAddress = (InetSocketAddress) receivedPacket.getSocketAddress();
					receivedMessage.put("clientSocketAddress", clientSocketAddress);

					//System.out.println("(Server) LOG: Message extracted. message : \""+receivedMessage+"\" handling message...");

					triggerEvent(receivedMessage);

					//System.out.println("(Server) LOG: Message handled. DONE");

				} catch (IOException e) {
					e.printStackTrace();
					socket.close();
					break;
				}
			}
		}
	}
	
	public static void triggerEvent(Message message){
		eventManager.handle(message);
	}
}
