package org.pogi.engine.net;

import java.awt.TrayIcon.MessageType;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.pogi.engine.net.constant.EventType;

/**
 * Just an abstraction of JSONObject, 
 * with a mandatory 'type' element 
 */
public class Message extends JSONObject {
	
	public Message(String source){
		super(source);
		try{
			get("type");
			put("type", EventType.valueOf(getString("type")) );
		}catch(JSONException e){
			e.printStackTrace();
			//System.out.println("Message must have a type");
		}
	}
	
	public Message(EventType type) {
		super();
		put("type", type);
	}

	public Message(EventType type, JSONTokener x) throws JSONException {
		super(x);
		put("type", type);
	}

	public Message(EventType type, Map arg0) {
		super(arg0);
		put("type", type);
	}

	public Message(EventType type, Object bean) {
		super(bean);
		put("type", type);		
	}

	public Message(EventType type, String source) throws JSONException {
		super(source);
		put("type", type);
	}

	public Message(EventType type, JSONObject arg0, String[] arg1) {
		super(arg0, arg1);
		put("type", type);
	}

	public Message(MessageType type, Object arg0, String[] arg1) {
		super(arg0, arg1);
		put("type", type);
	}

	public Message(EventType type, String arg0, Locale arg1) throws JSONException {
		super(arg0, arg1);
		put("type", type);
	}

}