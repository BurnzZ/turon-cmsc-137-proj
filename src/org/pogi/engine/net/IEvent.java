package org.pogi.engine.net;

/**
 * 
 * Interface for events
 *
 */
public interface IEvent {
	/**
	 * should contain the actions to be executed whenever this Event is triggered
	 * @param message message containing data relative to the actions
	 */
	public void apply(Message message);
}
