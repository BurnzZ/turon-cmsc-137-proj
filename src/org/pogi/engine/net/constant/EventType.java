package org.pogi.engine.net.constant;

/**
 *	Possible types of events that a message is made of.
 *	I don't if this should be named like this, or should be named "MessageType". Name = TBD?
 */
public enum EventType {
	CHAT, CONNECTION, HANDSHAKE, GAME_DATA, NEW_PLAYER, SOCKET_LIST, GAME_START, GAME_UPDATE,
	CHANGE_DIRECTION, OBJECT_SPAWN, BIKE_RESPAWN, PLAYER_DISCONNECTION, GAME_END, NEXT_ROUND_COUNTDOWN;
}