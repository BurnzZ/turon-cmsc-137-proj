package org.pogi.engine.net.constant;

/**
 * Constants for connection status
 */
public enum ConnectionStatus {
	CONNECTED, DISCONNECTED, RECONNECTED
}