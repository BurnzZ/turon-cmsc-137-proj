package org.pogi.engine.net.constant;

/**
 *	Constants for network related stuff.. as you can see
 */
public interface NetworkConstants {
	public static final int SERVER_PORT = 4445;
	public static final int GROUP_CLIENT_PORT = 6789;
	public static final int MIN_PORT = 49152; //random port to avoid conflict when using localhost
	public static final int MAX_PORT = 65535;
	public static final String GROUP_ADDRESS = "228.5.6.7";
}
