package org.pogi.game;

import java.util.List;

import org.json.JSONObject;
import org.pogi.engine.net.EventManager;
import org.pogi.engine.net.IEvent;
import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.Player;

/**
 * Implementation for EventManager for the client side.
 * Some events are just mimics/copies of server events to reflect changes in the game
 * @see EventManager
 */
public class ClientEventManager extends EventManager {

	@Override
	public void defineEvents() {
		//===========================		
		// GAME UPDATE EVENTS
		//===========================
		
		putEventHandler(EventType.GAME_START, new IEvent(){
			@Override
			public void apply(Message message) {
				GameManager gm = ((GameManager) Client.getGameManager());
				gm.startGameRendering();
			}
		});
		
		putEventHandler(EventType.GAME_END, new IEvent(){
			@Override
			public void apply(Message message) {
				//GameManager gm = ((GameManager) Client.getGameManager());
				//gm.startGameRendering();
			}
		});
		
		/*
		 * update the game data of the game manager
		 */
		putEventHandler(EventType.GAME_UPDATE, new IEvent(){
			@Override
			public void apply(Message message) {
				//System.out.println(message);
				
				GameManager gm = ((GameManager) Client.getGameManager());

				JSONObject data = message.getJSONObject("data");
				
				gm.applyGameData(data);
			}
		});
		
		//===========================		
		// OTHER EVENTS
		//===========================
		/*
		 * update the game data of the game manager
		 */
		putEventHandler(EventType.NEXT_ROUND_COUNTDOWN, new IEvent(){
			@Override
			public void apply(Message message) {
				int count = message.getInt("count");
				GameManager gm = ((GameManager) Client.getGameManager());
				gm.setNextRoundcount(count);
			}
		});
		
		/*
		 * update the GUI for the received chat message
		 */
		putEventHandler(EventType.CHAT, new IEvent(){
			@Override
			public void apply(Message message) {
				String username = message.getString("username");
				String chatBody = message.getString("message");
				
				String formattedString = String.format("%s: %s", username, chatBody);
				
				GameGUI.gotNewChatMessage(formattedString);
				
			}
		});
		
	}

}
