package org.pogi.game;
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.engine.net.javanet.Server;
import org.pogi.game.constant.Dimensions;
import org.pogi.game.constant.PanelCard;
import org.pogi.game.listener.ArrowKeyListener;
import org.pogi.game.listener.GameActionListener;

public class GameGUI {
	public static JFrame frame;
    //panel responsible for switching panels (cards)
	public static JPanel cards;
	//panel with the custom painted canvas for the main game
	public static GamePanel gamePanel;
    
    // These are involved in the game's menu UI
    private static MenuPanel menuPanel;
	
	// These are involved with the game's chat system components
	private static JPanel chatPanel;
	private static JTextArea  historyText;
    private static JTextField typedText;
	private static boolean showChatOverlay = false; // flag for showing the Chat Overlay
	
	static GameActionListener gameActionListener = new GameActionListener();
	
	
    private static void createAndShowGUI() {
        frame = new JFrame("Turon! tenetenenetenen!");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
            	GameManager gm =(GameManager) Client.getGameManager();
            	if(gm != null){
            		gm.disconnect(); //disconnect on close button
            	}
                System.exit(0);
            }
        } );
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();

    	kfm.setDefaultFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
    	kfm.setDefaultFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		
        
        // builds the gameplay field area
        gamePanel = GamePanel.getInstance();
        
        // builds the menu layout using the class's
        // constructor
        menuPanel = new MenuPanel();
        
        // builds the chat system
        setupChatPanel();


        // frame.add(menuPanel, BorderLayout.CENTER);
        
        //jpanel with card layout;
        cards = new JPanel(new CardLayout());
        cards.add(menuPanel, PanelCard.MENU);
        cards.add(gamePanel, PanelCard.GAME);
        
        bindKeysToCanvas();
        
        showMenu();
        frame.add(cards, BorderLayout.CENTER);
        frame.add(chatPanel, BorderLayout.EAST);

        frame.pack();
        frame.setSize(Dimensions.GAME_WIDTH, Dimensions.GAME_HEIGHT);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * This method setups the Chat panel UI that overlays
     * the existing menuPanel and gamePanel
     */
    public static void setupChatPanel() {
    	
    	chatPanel = new JPanel();
    	chatPanel.setPreferredSize(new Dimension(Dimensions.CHAT_WIDTH, Dimensions.GAME_HEIGHT));    	
    	chatPanel.setLayout(new BoxLayout(chatPanel, BoxLayout.Y_AXIS));
    	chatPanel.add(Box.createVerticalGlue());
    	chatPanel.setBackground(Color.orange);
    	
    	// shows all of the sent messages among the group of users
        historyText = new JTextArea(40, 11);
        historyText.setEditable(false);
        historyText.setBackground(Color.LIGHT_GRAY);
        chatPanel.add(new JScrollPane(historyText), BorderLayout.NORTH);
        
        // this is where the user inputs messages
        typedText = new JTextField(11);        
        typedText.setActionCommand("send chat"); //TODO refactor
        typedText.addActionListener(gameActionListener);
        typedText.setVisible(true);
        chatPanel.add(typedText, BorderLayout.SOUTH);
        
        typedText.setFocusable(true);
        chatPanel.setVisible(false);
    }
    
    
    /**
     * This method Toggles the visibility of the Chat system of the game
     */
    public static void toggleChatOverlay() {
		showChatOverlay = !showChatOverlay;
		
		// show chat Panel
		if (showChatOverlay) {
			typedText.requestFocusInWindow();
		}
		else {
			gamePanel.requestFocusInWindow();
		}
    }
    
    /**
     * show/switch the menu card panel
     */
    public static void showMenu(){
    	CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, PanelCard.MENU);
        chatPanel.setVisible(false);
    }
    
    /**
     * show/switch to the game card panel
     */
    public static void showGame(){
    	CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, PanelCard.GAME);
        chatPanel.setVisible(true);
        frame.setSize(Dimensions.GAME_WIDTH + Dimensions.CHAT_WIDTH,
        			  Dimensions.GAME_HEIGHT + Dimensions.BIKE_THICKNESS);
        
        
    }
    
    /**
     * This method is normally called to append the username and hostname
     * to the JFrame title bar.
     */
    public static void appendTextInFrame(String text, String type) {
    	
    	if (type.equals("username"))
        	frame.setTitle(frame.getTitle() + "  | username: " + text);
    
    	else if (type.equals("hostname")) 
        	frame.setTitle(frame.getTitle() + "  | hostname: " + text);

        menuPanel.requestFocus();
    }
 
    /**
     * Bind arrow keys to the game canvas
     */
    public static void bindKeysToCanvas(){
    	ArrowKeyListener akl = new ArrowKeyListener();
    	gamePanel.addKeyListener(akl);
    	typedText.addKeyListener(akl);
    }
    
    /**
     * simply appends to the entered text box with the given string
     * @param message string to append
     */
    public static void gotNewChatMessage(String message) {
		historyText.append(message + "\n");
    }
    
    public static String promptForInput(String variableName){
    	String str;
    	do {
    		str = JOptionPane.showInputDialog(frame, variableName+": "); 
            if(str == null) System.exit(0);
            else if (str.equals("")){
            	JOptionPane.showMessageDialog(frame,
                        "Please enter "+variableName, "Invalid "+variableName,
                        JOptionPane.ERROR_MESSAGE);
            }  
        }while(str.equals(""));
    	
    	appendTextInFrame(str, variableName);
    	
    	return str;
    }

    /**
     * Temporary buttons (actually, just one button) for host.
     * contains startGameButton that signals the host server to start the game
     */
	public static void showHostControlPanel() {
		JButton startGameButton = new JButton("Start Game");
		startGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Message gameDataRequestMessage = new Message(EventType.GAME_START);
				
				Server.triggerEvent(gameDataRequestMessage);
				((JButton) e.getSource()).setVisible(false);
			}
		});
		gamePanel.add(startGameButton);
	}

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GameGUI.createAndShowGUI();
                
                String username = promptForInput("username");
                Client.setUsername(username);
                Client.setEventManager(new ClientEventManager());
            }
        });
    }

}