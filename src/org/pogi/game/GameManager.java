package org.pogi.game;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.swing.Timer;

import org.json.JSONObject;
import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.engine.net.javanet.Server;
import org.pogi.game.constant.Direction;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.BikeSegment;
import org.pogi.game.entity.Player;
import org.pogi.game.entity.WallUp;

/**
 * The GameManager controls (manages) "most" the data in the game. {@link Server}
 * and {@link Client} must both have an instance of GameManager, because both
 * of them should have a copy of a game. Server executes Actions (actions alter
 * game data) and sends messages that 'mimics' the Actions for the Clients.
 * 
 * For now to 'mimic' means to copy paste both EventHandlers and their content 
 * from Client and Server. There should be an IAction interface so that they 
 * can just call the same action.
 * 
 * Note:
 * "most" - some objects control the data in the game as well.
 * I think I should refactor that, maybe make an IAction interface. next time.... 
 */
public class GameManager {
	private static final int NEXT_ROUND_COUNTDOWN_DURATION = 5;
	private TronGame tronGame;
	private Timer roundTimer;
	private Map<Bike, Integer> bikesToDestroyMap = new HashMap<Bike, Integer>();
	private Timer renderingTimer;
	private int nextRoundcount = -1;
	private Timer nextRoundTimer;
	private int consumedFoodCounter;
	private Timer slowDownTimer;
	private Bike slowPowerUpOwner;
	private boolean moveLater;

	public GameManager(){
		tronGame = new TronGame();
	}
	
	//====================================
	// METHODS FOR SERVER-USE ONLY
	//====================================
	
	/**
	 * Main Game logic per tick (per update)
	 * 
	 * Basically, the logic is this:
	 * Move all bikes forward and handle the 
	 * result of moving each bike forward.
	 * The result can be an Item that will be consumed,
	 * Or information about a collision that must be handled
	 * properly (bike collision, wall collision, self collision)
	 */
	public void updateGame(){
		List<Bike> bikes = tronGame.getBikes();
		Byte[][] board = tronGame.getBoard();
		
		if(slowPowerUpOwner == null)//update normally
		{
			for(Bike bike : bikes){
				if(Direction.valueOf(bike.getDirection()) == Direction.NONE) 
					continue;
				Object moveResult = bike.moveForward(board);
				
				//if the bike collided with an object, ie. not another bike
				if(moveResult instanceof Integer){
					int boardId = (int) moveResult;
					consumeItem(bike, boardId);
				}else{//if collided with another bike
					int[] collisionInfo = (int[]) moveResult;
					handleBikeCollision(bike, collisionInfo);
				}
			}
			
		}else{//slow down activated
			for(Bike bike : bikes){
				if(bike.equals(slowPowerUpOwner)){
					if(Direction.valueOf(bike.getDirection()) == Direction.NONE) 
						continue;
					Object moveResult = bike.moveForward(board);
					
					//if the bike collided with an object, ie. not another bike
					if(moveResult instanceof Integer){
						int boardId = (int) moveResult;
						consumeItem(bike, boardId);
					}else{//if collided with another bike
						int[] collisionInfo = (int[]) moveResult;
						handleBikeCollision(bike, collisionInfo);
					}
					
				}else if(moveLater){
					moveLater = false;
				}else{
					if(Direction.valueOf(bike.getDirection()) == Direction.NONE) 
						continue;
					Object moveResult = bike.moveForward(board);
					
					//if the bike collided with an object, ie. not another bike
					if(moveResult instanceof Integer){
						int boardId = (int) moveResult;
						consumeItem(bike, boardId);
					}else{//if collided with another bike
						int[] collisionInfo = (int[]) moveResult;
						handleBikeCollision(bike, collisionInfo);
					}
					moveLater = true;
					
				}
			}
			
		}
		
		destroyBikes();
		
	}
	
	/**
	 * Start the main game thread. 
	 * Thread: update game and send the update to the client
	 * this is only called by the server
	 * TODO: Thread should be in a separate file I think
	 */
	public void startGame() {
		new Thread(){
			final int GAME_SPEED = 24;
			public void run() {
				
				Message gameStartMessage = new Message(EventType.GAME_START);
				Server.broadcastMessage(gameStartMessage);
				
				tronGame.startGame();
				roundTimer = new Timer(1000, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						tronGame.setCurrentTimeForRound(tronGame.getCurrentTimeForRound() + 1);
						if(tronGame.getCurrentTimeForRound() == tronGame.getDurationPerRound()){
							endRound();							
							roundTimer.stop();
						}
					}
				});
				roundTimer.start();
				
				while(tronGame.isOnGoing() ){
					if(!tronGame.isPaused()){
						updateGame();
						sendGameUpdate();
					}
					try {	Thread.sleep(1000/GAME_SPEED); } catch (InterruptedException e) {}
				}
				
				Message gameEndMessage = new Message(EventType.GAME_END);
				Server.broadcastMessage(gameEndMessage);
			}
		}.start();
	}
	
	private void sendGameUpdate() {
		Message gameUpdateMessage = new Message(EventType.GAME_UPDATE);
		gameUpdateMessage.put("data", new JSONObject(tronGame));
		Server.broadcastMessage(gameUpdateMessage);
	};
	
	
	//===============================
	// ROUND SYSTEM METHODS
	//===============================
	
	/**
	 * This method is called only by the server; Initializes
	 * the server's game instance/copy.
	 */
	public void initRound(){
		tronGame.resetGameState();
		
		List<Player> players = tronGame.getPlayers();
		Byte[][] board = tronGame.getBoard();
		
		//System.out.println("(GameManager) LOG: \t creating player bike...");
		
		Random rand = new Random();
		int maxX = board[0].length;
		int maxY = board.length; 
		
		int randomX;
		int randomY;
		
		for(int i = 0; i < players.size(); i++){
			do{
				randomX = rand.nextInt(maxX);
				randomY = rand.nextInt(maxY);			
			}while(board[randomY][randomX] != 0);
			
			Bike bike = new Bike(new Point(randomX, randomY), players.get(i).getBoardId());
			tronGame.getBikes().add(bike);
			board[randomY][randomX] = bike.getBoardId();
			
			spawnRandomObject();
		}
	}
	
	/**
	 * restart game state and initiate a new round
	 */
	public void nextRound(){
		tronGame.nextRound();
		initRound();			
	}
	
	/**
	 * end the current round
	 */
	public void endRound(){
		//next round if not the last round; else end the game
		tronGame.pauseGame();
		saveRoundScores();
		sendGameUpdate(); //notify them that game has paused
		
		if(tronGame.getCurrentRound() + 1 < tronGame.getNumberOfRounds()){
			nextRoundcount = NEXT_ROUND_COUNTDOWN_DURATION;
			nextRoundTimer = new Timer(1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(nextRoundcount == 1){
						nextRound();
						tronGame.resumeGame();
						roundTimer.restart();
						nextRoundcount = -1; //nullify so it wont get rendered
						nextRoundTimer.stop();					
					}else{
						nextRoundcount--;
					}
					Message countDownMessage = new Message(EventType.NEXT_ROUND_COUNTDOWN);
					countDownMessage.put("count", nextRoundcount);
					Server.broadcastMessage(countDownMessage);
				}
			});
			nextRoundTimer.start();
			Message countDownMessage = new Message(EventType.NEXT_ROUND_COUNTDOWN);
			countDownMessage.put("count", nextRoundcount);
			Server.broadcastMessage(countDownMessage);
		}else{
			endGame();
		}
		
	}
	
	public void saveRoundScores(){
		List<Integer[]> roundScores = tronGame.getRoundScores();
		List<Player> players = tronGame.getPlayers();
		
		Integer[] currentRoundScores = new Integer[players.size()];
		for(int i = 0; i < currentRoundScores.length; i++){
			currentRoundScores[i] = players.get(i).getScore();
		}
		roundScores.add(currentRoundScores);
	}
	
	/**
	 * end the game (preferrably show results and such)
	 */
	public void endGame(){
		//TEMPORARY/PLACEHOLDER
		// JOptionPane.showMessageDialog(GameGUI.frame, "game has ended. results: <results here>");
		
		//===============================
		// END OF - ROUND SYSTEM RELATED METHODS
		//===============================
		
		GamePanel.setGameShowSummary(true);	
		GamePanel.setGameEnd();
	}
	
	
	
	//=================================
	// GAME/SERVER CONNECTION METHODS
	//=================================
	/**
	 * create a new Player. This is called when a client joins the host
	 * @param name name for the new player
	 */
	public void newPlayer(String name){
		Byte[][] board = tronGame.getBoard();
		
		byte boardId = (byte) (getGame().getPlayers().size() + 1);		
		
		//System.out.println("(GameManager) LOG: Adding new player...");
		
		Player player = new Player(name, boardId);
		tronGame.getPlayers().add(player);
		
		//System.out.println("(GameManager) LOG: new player added. Player list: ");
		//printPlayers();

	}
	
	/**
	 * Disconnect a player from the game
	 * 
	 * NOTE: this only disconnects the player from the game, not from the server, such that
	 * all references to the player are removed from the game state/data
	 * @param p player to disconnect from the game
	 * 
	 */
	public void disconnectPlayer(Player p){
		
		List<Bike> bikes = tronGame.getBikes();
		List<Player> players = tronGame.getPlayers();
		
		Bike bike = null;
		for(Bike b : bikes){
			if(p.getBoardId() == b.getBoardId()){
				bike = b;
				break;
			}
		}
		if(bike != null){
			destroyBike(bike);
			players.remove(p);
		}
	}
	
	
	/**
	 * Disconnect the client from the server, and ask/requests the server to disconnect the player
	 * from the game.
	 * 
	 * This method is preferably called upon exit of the application
	 * 
	 * NOTE: I am not sure if this method should be in Game Manager.
	 * Maybe I'll refactor this
	 */
	public void disconnect(){
		Message disconnectionMessage = new Message(EventType.PLAYER_DISCONNECTION);
		disconnectionMessage.put("username", Client.getUsername());
		Client.sendMessage(disconnectionMessage, Client.getServerAddress());
		Client.disconnect();
	}
	
	//=================================
	// END OF - GAME/SERVER DISCONNECTION METHODS
	//=================================
	
	/**
	 * spawn random object at the board
	 */
	public void spawnRandomObject(){
		Byte[][] board = tronGame.getBoard();
		
		int maxX = board[0].length;
		int maxY = board.length; 
		
		Random rand = new Random();
		
		//get random available x and y coordinates
		int randomX;
		int randomY;
		do{
			randomX = rand.nextInt(maxX);
			randomY = rand.nextInt(maxY);			
		}while(board[randomY][randomX] != 0);
		
		//get random powerup
		byte boardId;
		System.out.println(consumedFoodCounter);
		if((consumedFoodCounter + 1) % 5 == 0){
			int wallIncrease = rand.nextInt(10) + 1;
			boardId = (byte) (WallUp.boardId + wallIncrease);
		}else if((consumedFoodCounter + 1) % 12 == 0){
			boardId = 30;
		}else{
			boardId = WallUp.boardId;
		}
		
		
		board[randomY][randomX] = boardId;
		
	}
	
	/**
	 * Respawn bike method for the server
	 * @param p
	 */
	public void respawnBike(byte boardId){
		List<Bike> bikes = tronGame.getBikes();
		Byte[][] board = tronGame.getBoard();
		
		int maxX = board[0].length;
		int maxY = board.length; 
		
		Random rand = new Random();
		
		int randomX;
		int randomY;
		do{
			randomX = rand.nextInt(maxX);
			randomY = rand.nextInt(maxY);			
		}while(board[randomY][randomX] != 0);
		
		board[randomY][randomX] = boardId;
		bikes.add(new Bike(new Point(randomX, randomY), boardId));

	}
	
	//==========================
	// GAMEPLAY LOGIC METHODS
	//==========================
	
	public void consumeItem(Bike bike, int boardId){
		if(boardId == 0) return; //return if nothing to consume
		//apply effect of consumed object, if any
		if(boardId == -1){//wall collision
			bikesToDestroyMap.put(bike, 0);
			GamePanel.getInstance().playCollisionSound();
			return;
		}
		
		if(boardId == WallUp.boardId){ //normal food
			bike.addWallSizeIncreaseBy(1);
			GamePanel.getInstance().playNormalFoodConsumedSound();
		}else if(boardId > 10 && boardId <= 20){ //random-n food
			int wallSizeIncrease = boardId - WallUp.boardId;
			bike.addWallSizeIncreaseBy(wallSizeIncrease);
			int x = bike.getHead().getX()*20; //20 == thickness
			int y = bike.getHead().getY()*20 - 5;
			Color color = GamePanel.colorPlayersHUD[bike.getBoardId() - 1];
			GamePanel.getInstance().toast("+"+wallSizeIncrease, x, y, color);
			GamePanel.getInstance().playWildFoodConsumedSound();
		}else if(boardId == 30){ //slow time
			slowPowerUpOwner = bike;
			slowDownTimer = new Timer(10000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					slowPowerUpOwner = null;
					moveLater = false;
					slowDownTimer.stop();
				}
			});
			slowDownTimer.start();
			
			GamePanel.getInstance().playPurgeSound();
		}
		
		if(this.equals((GameManager) Server.getGameManager())){
			consumedFoodCounter++;		
			spawnRandomObject();
		}
		
	}
	
	public void handleBikeCollision(Bike bike, int[] collisionInfo){
		List<Bike> bikes = tronGame.getBikes();
		
		int x = collisionInfo[0];
		int y = collisionInfo[1];
		int boardId = collisionInfo[2];
		
		//if bike collided with another bike
		if(boardId < 10 && boardId > 0){

			Bike bikeHit = null;
			for(Bike bike2 : bikes){
				if(bike2.getBoardId() == boardId){
					bikeHit = bike2;
					break;
				}
			}
			
			BikeSegment collidedHead = bikeHit.getHead();
			
			if(collidedHead.getX() == x && collidedHead.getY() == y){
				bikesToDestroyMap.put(bike, 0); //no score; same as collision with walls
				bikesToDestroyMap.put(bikeHit, 0); //collided bike will also be destroyed;
			}else{
				bikesToDestroyMap.put(bike, boardId); //no score; same as collision with walls
			}
			
			GamePanel.getInstance().playCollisionSound();
		}
		
	}
	
	/**
	 * destroy all bikes that should be.
	 */
	public void destroyBikes(){
		List<Player> players = getGame().getPlayers();
		
		for(Entry<Bike, Integer> entry : bikesToDestroyMap.entrySet()){
			int objectHitBoardId = entry.getValue();
			Bike bike = entry.getKey();
			
			byte bikeBoardId = bike.getBoardId();
			destroyBike(bike);
			
			if(objectHitBoardId > 0){
				//determine player collided with
				Player p = null;
				for(Player player : players){
					if(player.getBoardId() == objectHitBoardId){
						p = player; 
						break;
					}
				}
				
				//if bike hit something
				if(objectHitBoardId != 0){
					if(objectHitBoardId != bike.getBoardId()){
						p.addScore(1);
					}
				}
			}
			
			respawnBike(bikeBoardId);
		}
		bikesToDestroyMap.clear();
	}
	
	/**
	 * destroy a bike and remove it from the board
	 * @param bike bike to destroy
	 */
	public void destroyBike(Bike bike){
		List<Bike> bikes = tronGame.getBikes();
		Byte[][] board = tronGame.getBoard();
		
		for(BikeSegment s : bike.getBody()){
			board[s.getY()][s.getX()] = 0;
		}
		bike.getBody().clear();
		bikes.remove(bike);
	}
	

	//=================================
	// METHODS FOR CLIENT-USE ONLY
	//=================================

	/**
	 * starts rendering for the game canvas
	 */
	public void startGameRendering(){
		final int MAX_FPS = 30;		
		renderingTimer = new Timer(1000/MAX_FPS, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//GAME_SPEED = MAX_FPS = 30 Frames per Second
				tronGame.startGame();
				if(tronGame.isOnGoing()){
					GameGUI.gamePanel.repaint();
				}else{
					renderingTimer.stop();
				}
			}
		});
		renderingTimer.start();
	}
	
	
	/**
	 * This is called by the Client, whenever a message containing {@link EventType#GAME_DATA} is received.
	 * This is preferably only called when : client initiates handshake to the server; game starts; client reconnects to
	 * the server (thus requests a full game update - not yet implemented)
	 * 
	 * @param data contains the data received from the server 
	 * for updating the game instance
	 */
	public void applyGameData(JSONObject data){
		tronGame = new TronGame(data);
	}
	
	//======================
	// GETTERS AND UTILS
	//======================
	
	public TronGame getGame(){
		return tronGame;
	}
	
	/**
	 * Print player list for debugging
	 */
	public void printPlayers(){
		List<Player> players = tronGame.getPlayers();
		for(int i = 0; i < players.size(); i++){
			System.out.println(players.get(i).getName()+" ,");
		}
	}
	
	/**
	 * prints the current board data for debugging
	 */
	public void printBoard(){
		Byte[][] board = tronGame.getBoard();
		
		for(int i = 0; i < board.length; i++){
			for(int j = 0; j < board[i].length ; j++){
				System.out.print(board[i][j]+" ");
			}
			System.out.println();
		}
	}

	public int getNextRoundcount() {
		return nextRoundcount;
	}

	public void setNextRoundcount(int nextRoundcount) {
		this.nextRoundcount = nextRoundcount;
	}
	
}
