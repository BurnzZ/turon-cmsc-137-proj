package org.pogi.game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.pogi.game.listener.MenuActionListener;
import org.pogi.game.constant.Dimensions;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel {

    private static JPanel manualPanel;
    private static JPanel menuItemsPanel;
    
    private static JLabel titleLabel;
    private static JLabel manualLabel;
    
    private static JButton hostButton;
    private static JButton joinButton;
    private static JButton helpButton;
    private static JButton exitBtn;
    
    
    private static Color colorBlue_center = new Color(200, 255, 255, 200);
    private static Color colorBlue_inner_light = new Color(0, 236, 239, 30);
    private static Color colorBlue_inner_glow = new Color(0, 236, 239, 20);
    private static Color colorBlue_outer_glow = new Color(0, 236, 239, 10);
    
	static MenuActionListener menuActionListener = new MenuActionListener();

	public MenuPanel() {
		this.setBackground(Color.black);
        
        
        menuItemsPanel = new JPanel();
        menuItemsPanel.setLayout(new BoxLayout(menuItemsPanel,BoxLayout.Y_AXIS));
        menuItemsPanel.setOpaque(false);
        
        menuItemsPanel.add(Box.createVerticalGlue());
        
        // GAME TITLE
        ImageIcon image = new ImageIcon(getClass().getResource("/res/title.png"));
        titleLabel = new JLabel(image);
        
        // Menu Items
        hostButton = createButtonImage("host_game.png");
        hostButton.setActionCommand("host game"); //TODO refactor
        hostButton.addActionListener(menuActionListener);

        joinButton = createButtonImage("join_game.png");
        joinButton.setActionCommand("join game"); //TODO refactor
        joinButton.addActionListener(menuActionListener);
        
        helpButton = createButtonImage("help.png");
        helpButton.setActionCommand("help"); //TODO refactor
        helpButton.addActionListener(menuActionListener);
        
        exitBtn = createButtonImage("exit.png");
        exitBtn.setActionCommand("exit game"); //TODO refactor
        exitBtn.addActionListener(menuActionListener);

        menuItemsPanel.add(titleLabel);
        menuItemsPanel.add(hostButton);
        menuItemsPanel.add(joinButton);
        menuItemsPanel.add(helpButton);
        menuItemsPanel.add(exitBtn);
        
        /* HELP MANUAL STUFF */
        
        manualPanel = new JPanel();
        manualPanel.setOpaque(false);
        manualPanel.setVisible(false);
        
        // adds a listener for when the manualPanel is clicked
        manualPanel.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent me) {
        		MenuPanel.hideManualPanel();
        	}
        });

        // manual image overlay
        ImageIcon image2 = new ImageIcon(getClass().getResource("/res/manual/manual.png"));
        manualLabel = new JLabel(image2);
        manualPanel.add(manualLabel);
        
        
        this.setFocusable(true);
        this.add(manualPanel);
        this.add(menuItemsPanel);
	}
	

	/*
	 * Called when the "Help" button is clicked,
	 * basically shows the Help Manual Panel
	 */
	public static void showManualPanel() {
        manualPanel.setVisible(true);
	}
	
	/*
	 * Called when the Help Manual Panel button is clicked,
	 * basically hides it
	 */
	public static void hideManualPanel() {
        manualPanel.setVisible(false);
	}
	
    /*
     * This method simplifies the process of adding images to the JButtons
     */
    public JButton createButtonImage(String filename) {
    	ImageIcon image = new ImageIcon(getClass().getResource("/res/" + filename));
    	JButton button = new JButton(image);
    	button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		return button;
    }
    

	/**
	 * This method is added to remove the aliasing in the
	 * Images in the Main Menu Screen
	 */
    public void setupAntiAliasing(Graphics2D g2) {
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
          RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
          RenderingHints.VALUE_RENDER_QUALITY);
    }
    
    /**
     * This method draws a line (using its 2 pairs of x-y coordinates),
     * in 4 different layers to simulate a glowing effect.  
     * The glowing effect relies heavily on the definition of colors used.
     * Each layer is for a distinct color, which is also applied by a specific stroke size.
     */
    public void renderGlowingLine(Graphics2D g2, int x1, int y1, int x2, int y2) {

        g2.setColor(colorBlue_outer_glow);
        g2.setStroke(new BasicStroke(16));
		g2.drawLine(x1, y1, x2, y2);
		
        g2.setColor(colorBlue_inner_glow);
        g2.setStroke(new BasicStroke(10));
		g2.drawLine(x1, y1, x2, y2);
		
        g2.setColor(colorBlue_inner_light);
        g2.setStroke(new BasicStroke(6));
		g2.drawLine(x1, y1, x2, y2);
		
        g2.setColor(colorBlue_center);
        g2.setStroke(new BasicStroke(1));
		g2.drawLine(x1, y1, x2, y2);
    }
    
    /**
     * This method is responsible for rendering the gridlines
     * in the Main Menu Screen
     */
    public void drawGrid(Graphics2D g2) {
    	
        for (int x=10; x<= Dimensions.GAME_WIDTH; x+=40) 
        	renderGlowingLine(g2, x, 0, x, Dimensions.GAME_WIDTH);
    	for (int y=10; y<=Dimensions.GAME_HEIGHT; y+=40) 
        	renderGlowingLine(g2, 0, y, Dimensions.GAME_WIDTH, y);
    }
	
	@Override
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		setupAntiAliasing(g2);
		
        //super.paintComponent(g2);
        
        drawGrid(g2);
        
    }
	
}
