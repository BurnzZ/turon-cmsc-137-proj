package org.pogi.game;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pogi.game.constant.Dimensions;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.Player;

/**
 * Data Model for the Tron Game
 * @see GameManager
 */
public class TronGame {
	boolean onGoing = false;
	int currentTimeForRound = 0;
	final int DURATION_PER_ROUND = 60;//in seconds

	int currentRound = 0;
	int numberOfRounds = 5;
	
	Byte[][] board;
	List<Player> players;
	List<Bike> bikes;
	
	List<Integer[]> roundScores; //roundScore[round][scores per player]
	private boolean paused;

	/**
	 * Constructor to be only used by server.
	 */
	public TronGame(){
		players = new ArrayList<Player>();
		bikes = new ArrayList<Bike>();
		roundScores = new ArrayList<Integer[]>();
		initBoard();
	}
	
	/**
	 * This is the constructor used exclusively by the Client. This is called
	 * every time a new update from the server is received.
	 * @param data data from server to be used to update the client game data
	 */
	public TronGame(JSONObject data){
		players = new ArrayList<Player>();
		bikes = new ArrayList<Bike>();
		roundScores = new ArrayList<Integer[]>();

		this.onGoing = data.getBoolean("onGoing");
		this.currentRound = data.getInt("currentRound");
		this.numberOfRounds = data.getInt("numberOfRounds");
		this.currentTimeForRound = data.getInt("currentTimeForRound");
		this.paused = data.getBoolean("paused");
		
		JSONArray jsonRoundScores = data.getJSONArray("roundScores"); 
	    for (int i = 0; i< jsonRoundScores.length(); i++){ 
	    	JSONArray jsonRoundScoresRow = jsonRoundScores.getJSONArray(i);
	    	Integer[] scoresForRound = new Integer[jsonRoundScoresRow.length()]; 
	    	for(int j = 0; j < scoresForRound.length ; j++){
	    		scoresForRound[j] = jsonRoundScoresRow.getInt(j);
	    	}
	    	roundScores.add(scoresForRound);
	    } 
		
		//System.out.println(data.toString());
		
		initBoard();
		JSONArray jsonBoard = data.getJSONArray("board"); 
		int numRows = jsonBoard.length();
	    for (int i = 0; i< numRows; i++){ 
	    	JSONArray jsonBoardRow = (JSONArray) jsonBoard.get(i);
	    	int numCols = jsonBoardRow.length();
			for(int j = 0; j < numCols; j++){
				board[i][j] = (byte) jsonBoardRow.getInt(j);
	    	}
	    } 
		
		JSONArray jsonPlayers = data.getJSONArray("players");
		for(int i = 0; i < jsonPlayers.length(); i++){
			players.add(new Player(jsonPlayers.getJSONObject(i)));
		}
		
		JSONArray jsonBikes = data.getJSONArray("bikes");
		for(int i = 0; i < jsonBikes.length(); i++){
			bikes.add(new Bike(jsonBikes.getJSONObject(i)));
		}
		
	}
	
	public List<Integer> getWinnerForRound(int roundNumber){
		roundNumber = roundNumber - 1;
		List<Integer> winnerIndicesList = new ArrayList<Integer>(); //list of index of player(s) that has/have the highest score
		Integer[] currentRoundScores = roundScores.get(roundNumber);
		for(int i = 0; i < currentRoundScores.length; i++){
			if(winnerIndicesList.size() > 0){
				int highestScorerIndex = winnerIndicesList.get(0);
				if(currentRoundScores[i] > currentRoundScores[highestScorerIndex]){
					winnerIndicesList.clear();
					winnerIndicesList.add(i);
				}else if(currentRoundScores[i] == currentRoundScores[highestScorerIndex]){
					winnerIndicesList.add(i);
				}
			}else{
				winnerIndicesList.add(i);
			}
		}
		/*
		if(winnerIndicesList.size() > 1){
			//draw between players
		}else{
			//only 1 winner
		}
		*/
		return winnerIndicesList;
		
	}
	
	
	public List<Integer> getUltimateWinner(){
		List<Integer> winnerIndicesList = new ArrayList<Integer>(); //list of index of player(s) that has/have the highest score
		
		Integer[] playerScoreSums = new Integer[roundScores.get(0).length];
		for(int i = 0; i < playerScoreSums.length; i++){
			playerScoreSums[i] = 0;
		}
		
		for(Integer[] scoresForRound : roundScores){
			for(int i = 0; i < scoresForRound.length; i++){
				playerScoreSums[i] += scoresForRound[i];
			}
		}
		
		for(int i = 0; i < playerScoreSums.length; i++){
			if(winnerIndicesList.size() > 0){
				int highestScorerIndex = winnerIndicesList.get(0);
				if(playerScoreSums[i] > playerScoreSums[highestScorerIndex]){
					winnerIndicesList.clear();
					winnerIndicesList.add(i);
				}else if(playerScoreSums[i] == playerScoreSums[highestScorerIndex]){
					winnerIndicesList.add(i);
				}
			}else{
				winnerIndicesList.add(i);
			}
		}
		
		return winnerIndicesList;
		
	}
	
	/**
	 * Reset the game for the next round
	 */
	public void resetGameState(){
		currentTimeForRound = 0;
		initBoard();
		bikes.clear();
	}

	/**
	 * initialize the board elements to zero
	 */
	public void initBoard(){
		board = new Byte[Dimensions.FIELD_HEIGHT][Dimensions.FIELD_WIDTH];
		for(int i = 0; i < board.length; i ++){
			for(int j = 0; j < board[i].length; j++){
				board[i][j] = 0;
			}
		}
	}
	
	//=======================
	// GETTERS AND SETTERS
	//
	// Why the need for All of these getters?
	// getters are needed for construction
	// of a JSONObject from a Java Object in order
	// to map the field properly in the JSONObject instance,
	// as specified in the org.json library
	//========================
	
	public Byte[][] getBoard() {
		return board;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public List<Bike> getBikes() {
		return bikes;
	}
	
	public List<Integer[]> getRoundScores(){
		return roundScores;
	}
	
	public void nextRound(){
		currentRound++;
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public int getNumberOfRounds() {
		return numberOfRounds;
	}
	
	public boolean isOnGoing(){
		return this.onGoing;
	}
	
	public void startGame(){
		this.onGoing = true;
	}
	
	public void endGame(){
		this.onGoing = false;
	}
	
	public void pauseGame(){
		this.paused = true;
	}
	
	public void resumeGame(){
		this.paused = false;
	}
	
	public boolean isPaused(){
		return this.paused;
	}
	
	public int getCurrentTimeForRound(){
		return currentTimeForRound;
	}
	
	public void setCurrentTimeForRound(int time){
		this.currentTimeForRound = time;
	}
	
	public int getDurationPerRound(){
		return DURATION_PER_ROUND;
	}
	
	//=====================
	//	UTILS
	//=====================
	public void printBoard(){
		for(int i = 0; i < board.length; i ++){
			for(int j = 0; j < board[i].length; j++){
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}
}
