package org.pogi.game;

import java.awt.Graphics;

public abstract class Toast {
	private Long duration;
	private Long timeStarted;
	
	public Toast(long duration){
		this.duration = duration;
	}
	
	public boolean hasNextTick(){
		if(timeStarted == null){
			return true;
		}else{
			long timeElapsed = System.currentTimeMillis() - timeStarted;
			return timeElapsed < duration;
		}
	}
	
	public void tick(Graphics g){
		if(timeStarted == null){
			timeStarted = System.currentTimeMillis();
		}else{
			long timeElapsed = System.currentTimeMillis() - timeStarted;
			onTick(g, timeElapsed, duration);
		}
	}
	
	public long getTimeDuration(){
		return duration;
	}
	
	public abstract void onTick(Graphics g, long timeElapsed, long duration);
}
