package org.pogi.game.constant;

/*
 * Constants for directions
 */
public enum Direction {
	UP, DOWN, LEFT, RIGHT, NONE;
	
	 public static Direction getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
