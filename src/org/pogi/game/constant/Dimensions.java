package org.pogi.game.constant;

public interface Dimensions {
	// The size of the playing area (game panel)
	public static int GAME_WIDTH = 1080;
	public static int GAME_HEIGHT = 640;
	
	// the array size of the field in which the bikes traverses
	public static int FIELD_WIDTH = 54;
	public static int FIELD_HEIGHT = 32;
	
	// thickness of the bike rendered in the screen
	public static int BIKE_THICKNESS = 20;
	
	// width of the chat panel beside the game panel
	public static int CHAT_WIDTH = 360;
}
