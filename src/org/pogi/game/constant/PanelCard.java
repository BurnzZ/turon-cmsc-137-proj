package org.pogi.game.constant;

/**
 * JPanel Card Identifiers
 */
public interface PanelCard {
	public static final String MENU = "Menu Panel";
	public static final String GAME = "Game Panel";
}
