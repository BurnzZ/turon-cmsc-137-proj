package org.pogi.game.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.pogi.engine.net.javanet.Client;
import org.pogi.engine.net.javanet.Server;
import org.pogi.game.GameGUI;
import org.pogi.game.GameManager;
import org.pogi.game.MenuPanel;
import org.pogi.game.ServerEventManager;

/**
 * Listens to Actions in the Menu Panel, for hosting, joining, and exiting the game
 */
public class MenuActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		String actionCommand = e.getActionCommand();
		
		switch(actionCommand){ //TODO refactor the actionCommands
		case "host game":
    		Server.setGameManager(new GameManager());
    		Server.setEventManager(new ServerEventManager());
    		Server.run();
    		
			Client.setGameManager(new GameManager());
    		Client.joinHost(Server.getAddress());
    		GameGUI.appendTextInFrame(Server.getAddress(), "hostname");
    		
    		GameGUI.showGame();    		
    		GameGUI.showHostControlPanel();
    		
			break;
		case "join game":
			String hostname = GameGUI.promptForInput("hostname");
			
			Client.setGameManager(new GameManager());
			Client.joinHost(hostname);
			
			GameGUI.showGame();
			
			break;
			
		case "help":
			
			//GameGUI.showGame();
			MenuPanel.showManualPanel();
			
			break;
		case "exit game":
			System.exit(0);
			break;
			default: System.out.println("(MenuActionListener) invalid action command");
		}
	}
	
}
