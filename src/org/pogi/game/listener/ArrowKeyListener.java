package org.pogi.game.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.game.GameGUI;
import org.pogi.game.GameManager;
import org.pogi.game.constant.Direction;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.Player;


public class ArrowKeyListener implements KeyListener {

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@hello@@@@@@@@@@@@@@@@@@@@");
		
		int code = e.getKeyCode();  // This code tells which key was pressed.  The value is one of the 
		        // virtual keyboard ("VK") constants in the KeyEvent class.
		Direction newDirection = null;
		
		if (code == KeyEvent.VK_LEFT)
			newDirection = Direction.LEFT;
		else if (code == KeyEvent.VK_RIGHT)
			newDirection = Direction.RIGHT;
		else if (code == KeyEvent.VK_UP)
			newDirection = Direction.UP;
		else if (code == KeyEvent.VK_DOWN)
			newDirection = Direction.DOWN;
		
		else if (code == KeyEvent.VK_TAB){
			GameGUI.toggleChatOverlay();
			return;
		}

		if(newDirection == null) return;
		
		GameManager gm = (GameManager) Client.getGameManager();
		List<Player> players = gm.getGame().getPlayers();
		
		Player player = null;
		for(Player p : players){
			if(p.getName().equals(Client.getUsername())){
				player = p;
				break;
			}
		}
		
		List<Bike> bikes = gm.getGame().getBikes();
		Bike bike = null;
		for(Bike b : bikes){
			if(player.getBoardId() == b.getBoardId()){
				bike = b;
				break;
			}
		}

		if(bike == null){
			return;
		}
		Direction currentDirection = Direction.valueOf(bike.getDirection());
		
		//determine if the player can change direction
		boolean canChangeDirection = false;
		
		if(gm.getGame().isPaused()){
			canChangeDirection = false;
		}
		else if(bike.getBody().size() > 1){
			switch(newDirection){
			case UP:
				canChangeDirection = !currentDirection.equals(Direction.DOWN);
				break;
			case DOWN:
				canChangeDirection = !currentDirection.equals(Direction.UP);
				break;
			case LEFT:
				canChangeDirection = !currentDirection.equals(Direction.RIGHT);
				break;
			case RIGHT:
				canChangeDirection = !currentDirection.equals(Direction.LEFT);
				break;
			}
				
		}else{
			canChangeDirection = true;
		}
		
		if(canChangeDirection){
			Message changeDirectionMessage = new Message(EventType.CHANGE_DIRECTION);
			changeDirectionMessage.put("username", Client.getUsername());
			changeDirectionMessage.put("direction", newDirection);
			Client.sendMessage(changeDirectionMessage, Client.getServerAddress());
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
