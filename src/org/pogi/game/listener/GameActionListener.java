package org.pogi.game.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;

/**
 * Listens to Game Related Actions triggered by {@link JButton}s
 */
public class GameActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		String actionCommand = e.getActionCommand();
		
		/*
		 * Send a chat message to the server. (chat message will be broadcasted to all by the server)
		 */
		switch(actionCommand){ //TODO refactor the actionCommands
		case "send chat":
			JTextField typedText = (JTextField) e.getSource();
			
			if(typedText.equals("")){
				break;
			}
			
			Message chatMessage = new Message(EventType.CHAT);
			chatMessage.put("message",typedText.getText());
			chatMessage.put("username", Client.getUsername());
			
			Client.sendMessage(chatMessage, Client.getServerAddress());
			
			typedText.setText("");
			break;
			default: System.out.println("(GameActionListener) Error: invalid action command");
		}
	}

}
