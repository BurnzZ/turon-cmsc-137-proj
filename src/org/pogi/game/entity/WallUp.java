package org.pogi.game.entity;

public class WallUp {
	public static final int boardId = 10;

	public static void applyEffect(Bike bike) {
		bike.addWallSizeIncreaseBy(1);
	}
}