package org.pogi.game.entity;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pogi.game.GameManager;
import org.pogi.game.TronGame;
import org.pogi.game.constant.Direction;

/**
 * A bike that is controlled by a player.
 * A bike contains bike segments that are reflected
 * in the game board. This moves forward every frame
 * in its current direction
 * 
 * @see BikeSegment
 */
public class Bike {
	byte boardId;
	List<BikeSegment> body;
	BikeSegment head;
	BikeSegment tail;
	String direction; //direction snake is heading towards
	int wallSizeIncrease;
	
	/**
	 * Constructor preferably used by the server
	 * @param point initial point in the board
	 * @param boardId identifier for rendering; equivalent value of a {@link BikeSegment} in the {@link TronGame#board}
	 */
	public Bike(Point point, int boardId){
		this.boardId = (byte) boardId;
		body = new LinkedList<BikeSegment>();
		body.add(new BikeSegment(point.x, point.y));
		
		direction = Direction.NONE.toString();

		head = body.get(0);
		tail = body.get(0);
		
		//System.out.println(direction.toString());
	}
	
	/**
	 * Constructor preferably used by the client.
	 * @param data contains data for construction of the bike; received from the server
	 */
	public Bike(JSONObject data){
		boardId = (byte) data.getInt("boardId");
		direction = data.getString("direction");
		body = new LinkedList<BikeSegment>();
		
		JSONArray JSONBody = data.getJSONArray("body");
		for (int i=0; i< JSONBody.length(); i++) {
			body.add( new BikeSegment( JSONBody.getJSONObject(i)) );
		}
		
		tail = body.get(0);
		head = body.get(body.size() - 1);

	}
	
	/**
	 * Move forward by 1 step in the current bike's directions
	 * @param gameManager 
	 */
	public Object moveForward(Byte[][] board){	
		int newX = -1, newY = -1;
		
		switch(Direction.valueOf(direction)){
		case UP:
			if(head.getY() - 1 > -1){
				newX = head.getX();
				newY = head.getY() - 1;
			}
			break;
		case DOWN:
			if(head.getY() + 1 < board.length){
				newX = head.getX();
				newY = head.getY() + 1;
			}
			break;
		case LEFT:
			if(head.getX() - 1 > -1){
				newX = head.getX() - 1;
				newY = head.getY();
			}
			break;
		case RIGHT:
			if(head.getX() + 1 < board[0].length){
				newX = head.getX() + 1;
				newY = head.getY();
			}
			break;
		}
		
		//ensure bike has moved forward
		if(newX != -1 && newY != -1){
			//get current value at destination before overwriting
			int valueAtDestination = board[newY][newX];
			
			//move forward and grow if possible
			if(valueAtDestination == 0 || valueAtDestination >= 10){
				//create a new bike segment and replace the value at the destination with the bike segment's boardId
				body.add(new BikeSegment(newX, newY));
				board[newY][newX] = boardId;
				head = body.get(body.size() - 1);			
				
				//at this point: value is consumed
				
				//increase wall size if necessary
				if(wallSizeIncrease == 0){
					removeSegment(tail);
					board[tail.getY()][tail.getX()] = 0;
					tail = body.get(0);
				}else{
					wallSizeIncrease--;
				}
				
				return valueAtDestination;
			}else{
				return new int[]{newX, newY, valueAtDestination};
			}
			
		}else{//collision with wall
			return -1;
		}
	}
	
	public int getWallSizeIncrease(){
		return this.wallSizeIncrease;
	}
	
	public void addWallSizeIncreaseBy(int val)
	{
		this.wallSizeIncrease = val;
	}
	/**
	 * Removes a segment from the body
	 */
	public void removeSegment(BikeSegment s){
		body.remove(s); //pop tail
				
	}

	//=======================================
	//	GETTERS AND SETTERS
	//=======================================
	
	/**
	 * change the direction for moving forward
	 */
	public void setDirection(String dir){
		direction = dir;
	}
	
	public String getDirection(){
		return direction;
	}
	
	public byte getBoardId() {
		return boardId;
	}
	
	public List<BikeSegment> getBody() {
		return body;
	}

	public BikeSegment getHead() {
		return head;
	}

	
	
	
	
	
}
