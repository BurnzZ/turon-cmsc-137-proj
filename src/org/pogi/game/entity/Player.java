package org.pogi.game.entity;

import org.json.JSONObject;

/**
 * Model for the player
 * TODO: score, lives, powerup Fields. TBD
 */
public class Player {
	String name;
	private byte boardId;
	private int score;
	
	public Player(String name, byte boardId){
		this.name = name;
		this.boardId = boardId;
	}
	
	public Player(JSONObject data){
		this.name = data.getString("name");
		this.boardId = (byte) data.getInt("boardId");
		this.score = data.getInt("score");
	}
	
	public byte getBoardId(){
		return boardId;
	}
	
	public String getName(){
		return name;
	}
	public void addScore(int scoreToAdd) {
		score += scoreToAdd;
	}
	
	public int getScore(){
		return score; 
	}
}
