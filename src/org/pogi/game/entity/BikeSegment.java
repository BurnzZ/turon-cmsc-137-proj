package org.pogi.game.entity;

import org.json.JSONObject;

/**
 * A segment of a bike, containing a Point position
 * @see Bike
 */
public class BikeSegment{
	private int x;
	private int y;
	private String orientation = "";
	
	public BikeSegment(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public BikeSegment(JSONObject jsonObject) {
		this.x = jsonObject.getInt("x");
		this.y = jsonObject.getInt("y");
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}

	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
}