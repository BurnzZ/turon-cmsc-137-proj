package org.pogi.game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.pogi.engine.net.javanet.Client;
import org.pogi.game.constant.Dimensions;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.BikeSegment;
import org.pogi.game.entity.Player;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements MouseListener {
	List<Toast> toastList = new ArrayList<Toast>();
	Font toastFont = new Font("Serif", Font.BOLD, 24);
	public static GamePanel INSTANCE;
	
	public static GamePanel getInstance(){
		if(INSTANCE == null){
			INSTANCE = new GamePanel();
		}
		return INSTANCE;
	}
	
	// This array contains the defined colors for
	// displaying player info in the HUD
	public static Color colorPlayersHUD[] = {
		new Color(255,0,0),
		new Color(0,255,0),
		new Color(0,0,255),
		new Color(255,131,0)
	};
	
	// food sprites
	ImageIcon food_blue = new ImageIcon(getClass().getResource("/res/sprites_powerup/food.png"));
	ImageIcon food_time = new ImageIcon(getClass().getResource("/res/sprites_powerup/time.png"));
	int foodRandomAnimationIndex;
	ImageIcon[] food_random = new ImageIcon[]{
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num1.png")),
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num2.png")),
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num3.png")),
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num4.png")),
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num5.png")),
			new ImageIcon(getClass().getResource("/res/sprites_powerup/food_with_num6.png")),
	};

	
	//glow sprites R G B Cyan
	private ImageIcon[][] glowImages = {
		new ImageIcon[]{
				new ImageIcon(getClass().getResource("/res/sprites_glow/red_uni.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/red_horizontal.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/red_vertical.png"))
			},
		new ImageIcon[]{
				new ImageIcon(getClass().getResource("/res/sprites_glow/green_uni.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/green_horizontal.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/green_vertical.png"))
			},
		new ImageIcon[]{
				new ImageIcon(getClass().getResource("/res/sprites_glow/blue_uni.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/blue_horizontal.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/blue_vertical.png"))
			},
		new ImageIcon[]{
				new ImageIcon(getClass().getResource("/res/sprites_glow/cyan_uni.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/cyan_horizontal.png")),
				new ImageIcon(getClass().getResource("/res/sprites_glow/cyan_vertical.png"))
			}
	};
	
	private ImageIcon[] bikeImages = {
		new ImageIcon(getClass().getResource("/res/sprites_bike/red_bike.png")),
		new ImageIcon(getClass().getResource("/res/sprites_bike/green_bike.png")),
		new ImageIcon(getClass().getResource("/res/sprites_bike/blue_bike.png")),
		new ImageIcon(getClass().getResource("/res/sprites_bike/cyan_bike.png"))
	};
	
	private static Color colorBackgroundSummary = new Color(0, 0, 0, 100);
    private static Color colorBlue_center = new Color(200, 255, 255, 30); 
    private static Color colorBlueTron = new Color(100, 255, 255, 255);
    private static Color colorSpectator = new Color(150, 150, 150, 255);
	
	// These define the font used for
	// drawing the player infos in the HUD
	private Font fontPlayerName;
	private Font fontPlayerScore;
	
	private static boolean gameShowSummary = false;
	private static boolean gameEnded = false;
    AudioInputStream audioIn;
	
	public void playNormalFoodConsumedSound(){
		try{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(getClass().getResource("/res/sfx/smb2_coin.wav"));
            Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public void playWildFoodConsumedSound(){
		try{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(getClass().getResource("/res/sfx/smb2_grow.wav"));
            Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public void playCollisionSound(){
		try{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(getClass().getResource("/res/sfx/glass_break.wav"));
            Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public void playPurgeSound(){
		try{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(getClass().getResource("/res/sfx/smb2_purge.wav"));
            Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public GamePanel() {
        this.setBackground(Color.black);
        this.setFocusable(true);
        
        fontPlayerName = retrieveTronFont();
    	fontPlayerScore = new Font("Courier New", Font.BOLD, 20);
	}
	
	/**
	 * This method is added to remove the aliasing in the
	 * Images in the Main Menu Screen
	 */
    public void setupAntiAliasing(Graphics2D g2) {
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
          RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
          RenderingHints.VALUE_RENDER_QUALITY);
    }
	
    /**
     * This method retrieves the "TRON" font located at
     * ./res/T2rn.ttf. If not successfully found, "Courier New"
     * is returned, along with the defined Font style and size.
     * @return
     */
    public Font retrieveTronFont() {
    	try {
            //Returned font is of pt size 1
            Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getClassLoader().getResourceAsStream("res/Tr2n.ttf"));

            //Derive and return a 12 pt version:
            //Need to use float otherwise
            //it would be interpreted as style
            
            return font.deriveFont(12f).deriveFont(40f);

        } catch (Exception e) {
            // Handle exception
    	    e.setStackTrace(null);
        }
    	
    	// returns default font just in case
    	return new Font("Courier New", Font.BOLD, 15);
    }
    
    public void toast(final String message, final int x, final int y, final Color color){
    	toastList.add(new Toast(1000) {
			@Override
			public void onTick(Graphics g, long timeElapsed, long duration) {
				double percentToDuration = ((double) timeElapsed)/((double) duration);
				int moveUpHeight = 10;
				Color origColor = g.getColor();
				Font origFont = g.getFont();
				g.setFont(toastFont);
				g.setColor(color);
				g.drawString(message, x,(int) (y - moveUpHeight*percentToDuration));
				g.setColor(origColor);
				g.setFont(origFont);
			}
		});
    }
    
    /**
     * This method is responsible for drawing the players' bikes and their
     * corresponding segments in the game field.
     * Moreover, all of the game field's area is repainted with black to
     * erase all of the past bikes' movements. Any drawing of components
     * must be done after this method has been called to prevent :over-painting:.
     */
    public void drawBikes(Graphics g, Graphics2D g2) {
    
    	// gets the CONSTANT for the bike's thickness
		int thickness = Dimensions.BIKE_THICKNESS;
    	
		GameManager gm = ((GameManager) Client.getGameManager());
		
    	Byte[][] board = ((GameManager) Client.getGameManager()).getGame().getBoard();
		g2.setColor(Color.black);   
		//draw black board
		g2.fillRect(0, 0, Dimensions.FIELD_WIDTH + thickness, Dimensions.FIELD_HEIGHT + thickness);
		
		//draw foods
		for (int y=0; y<Dimensions.FIELD_HEIGHT; y++) {
			for (int x=0; x<Dimensions.FIELD_WIDTH; x++) {
				if (board[y][x] >= 10) {
					// for background default
					g.setColor(Color.black);     
					
					// SPRITE: for the normal food
					if (board[y][x] == 10) {
						g2.drawImage(food_blue.getImage(), x*thickness, y*thickness, this);
					}else if(board[y][x] > 10 && board[y][x] <= 20){
						//SPRITE: for the random-n food
						g2.drawImage(food_random[foodRandomAnimationIndex++].getImage(), x*thickness, y*thickness, this);
						if(!(foodRandomAnimationIndex < 6)){
							foodRandomAnimationIndex = 0;
						}
					}else if(board[y][x] == 30){
						g2.drawImage(food_time.getImage(), x*thickness, y*thickness, this);
					}
				}
			}
		}
		
		//set line (the wall) width
		Stroke origStroke = g2.getStroke();
		g2.setStroke(new BasicStroke(2));
		
		//draw bike walls and the glow for each bike segment
		List<Bike> bikes = gm.getGame().getBikes();
		for(Bike bike : bikes){
			List<BikeSegment> body = bike.getBody();
			g2.setColor(colorPlayersHUD[bike.getBoardId()-1]);
			
			int x1 = 0;
			int y1 = 0;
			int x2 = 0;
			int y2 = 0;
			for(int i = 0; i < body.size() - 1; i++){
				BikeSegment s1 = body.get(i);
				BikeSegment s2 = body.get(i+1);
				
				//get the orientation
				int xDiff = Math.abs(s1.getX() - s2.getX());
				String spreadAxis = xDiff == 0? "x":"y";
				
				//set point initially at middle
				x1 = s1.getX()*thickness + thickness/2;
				y1 = s1.getY()*thickness + thickness/2;
				x2 = s2.getX()*thickness + thickness/2;
				y2 = s2.getY()*thickness + thickness/2;
				
				boolean isCorner = false;
				//if corner if the segment is at least not the tail
				//determine if the segment is a corner
				BikeSegment previousSegment = null;
				if(i > 0){
					previousSegment = body.get(i - 1);
					//check orientation to know if corner or not
					int xDiff2 = Math.abs(s1.getX() - previousSegment.getX());
					String spreadAxis2 = xDiff2 == 0? "x":"y";
					if(!spreadAxis2.equals(spreadAxis)){
						isCorner = true;
					}
				}
				
				//draw corner, horizontal, or vertical image
				if(isCorner){
					Image cornerGlowImage = glowImages[bike.getBoardId()-1][0].getImage();
					g2.drawImage(cornerGlowImage, s1.getX()*thickness, s1.getY()*thickness, null);
				}else{
					Image cornerGlowImage;
					if(spreadAxis.equals("x")){
						cornerGlowImage = glowImages[bike.getBoardId()-1][2].getImage();
					}else{
						cornerGlowImage = glowImages[bike.getBoardId()-1][1].getImage();
					}
					g2.drawImage(cornerGlowImage, s1.getX()*thickness, s1.getY()*thickness, null);					
				}
				
				//spreadFactor depends on the distance of the segment from the head
				int spreadFactor;
				spreadFactor = (int) Math.min(body.indexOf(s1)/2, (thickness/2)*0.25);
				
				//spread the lines depending on the spread axis
				switch(spreadAxis){
				case "x":
					g2.drawLine(x1-spreadFactor, y1, x2-spreadFactor, y2);
					g2.drawLine(x1+spreadFactor, y1, x2+spreadFactor, y2);
					break;
				case "y":
					g2.drawLine(x1, y1-spreadFactor, x2, y2-spreadFactor);
					g2.drawLine(x1, y1+spreadFactor, x2, y2+spreadFactor);

					break;
				}

			}
			
			//draw the head (the bike image)
			Image bikeImage = bikeImages[bike.getBoardId()-1].getImage();
			BikeSegment head = body.get(body.size() - 1);
			
			int directionFactor = 0;
			switch (bike.getDirection()) {
				case "UP": directionFactor = 0;
					break;
				case "LEFT": directionFactor = 1;
					break;
				case "DOWN": directionFactor = 2;
					break;
				case "RIGHT": directionFactor = 3;
					break;
			};
			
			AffineTransform at = new AffineTransform(); //transform matrix for rotation
			at.translate(head.getX()*20 + thickness/2, head.getY()*20 + thickness/2);	//translate to the position on board					
			at.rotate(Math.toRadians(90*directionFactor));	//rotate 90 degrees
		    at.translate(-thickness/2, -thickness/2);		//translate again such that it rotates on its center
			g2.drawImage(bikeImage, at, null);	//draw the image
			
		}
		
		//set back original stroke
		g2.setStroke(origStroke);
    }

    /**
     * This method is responsible for rendering the gridlines
     * in the Main Menu Screen
     */
    public void drawGrid(Graphics g) {
    	
    	g.setColor(colorBlue_center);
    	
        for (int x=10; x<= Dimensions.GAME_WIDTH; x+=20) 
        	g.drawLine(x, 0, x, Dimensions.GAME_HEIGHT);
    	for (int y=10; y<=Dimensions.GAME_HEIGHT; y+=20) 
        	g.drawLine(0, y, Dimensions.GAME_WIDTH, y);
    }
    
    /**
     * This method draw the "Heads Up Display" or "HUD" in the gameplay area.
     * The HUD shows the currently connected players, as well as their corresponding
     * game scores and other info.
     */
    public void drawHUD(Graphics g) {
    	List<Toast> doneToastList = new ArrayList<Toast>();
    	for(Toast toast : toastList){
    		if(toast.hasNextTick()){
    			toast.tick(g);
    		}else{
    			doneToastList.add(toast);
    		}
    	}
    	toastList.removeAll(doneToastList);
    	// gets the current game entities
    	TronGame game = ((GameManager) Client.getGameManager()).getGame();
		List<Player> players = game.getPlayers();
		List<Bike> bikes = game.getBikes();
		
		// flag for determining if the current user is a spectator
		boolean clientIsPlaying = false;
		
		//paint countdown timer for next round if there is any
		GameManager gm = ((GameManager) Client.getGameManager());
		if(gm.getNextRoundcount() != -1){
			
			setGameShowSummary(true);
			g.setColor(Color.WHITE);
			g.drawString("Starting next round in "+gm.getNextRoundcount()+"...",
					Dimensions.GAME_WIDTH/2 - 50,
					Dimensions.GAME_HEIGHT/7);
		}
		else if (gameEnded)
			setGameShowSummary(true);
		else
			setGameShowSummary(false);
		
		
		// cross checks who is currently playing
		int index = 0;
		List<Integer> playingPlayers = new ArrayList<Integer>();
		for(Bike b : bikes) {
			for(Player p : players) {
				if (b.getBoardId() == p.getBoardId()) {
					playingPlayers.add(index); // specifies the index of the playing player
					
					if(p.getName().equals(Client.getUsername()))
						clientIsPlaying = true;
					
					continue;
				}
			}
			index++;
		}
		
		// not spectator
		if(!clientIsPlaying) {
			
			g.setColor(colorSpectator);
			g.setFont(fontPlayerName);
			
			// draws the info regarding Spectatorship
			g.drawString("< SPECTATOR MODE >",
						 ((Dimensions.GAME_WIDTH/4) + 75),
						 0 + 40);
		}
			
		// prints the connected players name in the game
		// with their specific info
		for (int i=0; i< players.size(); i++) {
			g.setColor(colorPlayersHUD[i]);
			
			// determines if player is spectator
			boolean isSpectator = true;
			for (Integer p : playingPlayers) {
				if (p == i) {
					isSpectator = false;
					break;
				}
			}
			
			// changes color to grey if spectator
			if (isSpectator) {
				g.setFont(fontPlayerScore);
				g.setColor(colorSpectator);
				g.drawString("< SPECTATOR >",
				     	((Dimensions.GAME_WIDTH / 8) * (i*2)) + 50,
				     	Dimensions.GAME_HEIGHT - 90);
			}
			
			// draws the player's name
			g.setFont(fontPlayerName);
			g.drawString(players.get(i).getName(),
				     	((Dimensions.GAME_WIDTH / 8) * (i*2)) + 50,
				     	Dimensions.GAME_HEIGHT - 50);
			
			// draws the player's score
			g.setFont(fontPlayerScore);
			g.drawString("Score: " + players.get(i).getScore(),
				     	((Dimensions.GAME_WIDTH / 8) * (i*2)) + 50,
				     	Dimensions.GAME_HEIGHT - 30);
		}
		
    }
    
    /**
     * This method shows the Game's Summary (final scores of all players)
     * at the end of all the Game's round
     */
    public void drawGameSummary(Graphics g) {
    	
    	
    	TronGame game = ((GameManager) Client.getGameManager()).getGame();
		List<Player> players = game.getPlayers();
		List<Integer[]> roundScores = game.getRoundScores();
		
		// show transparent black rectangle overlay as background
    	g.setColor(colorBackgroundSummary);
    	g.fillRect(100, 100, Dimensions.GAME_WIDTH - 200, Dimensions.GAME_HEIGHT - 200);
  	
		
		// show "GAME SUMMARY" header
		g.setFont(fontPlayerName);
		g.setColor(Color.WHITE);
		g.drawString("GAME SUMMARY", (Dimensions.GAME_WIDTH / 2) - 125, 150);
		
		// show rounds header
    	for (int i=0; i < 5; i++) {
			g.setFont(fontPlayerScore);
			g.drawString("ROUND " + (i+1),
						((Dimensions.GAME_WIDTH - 200)/5) + 170 + (i*130),
						200);
    	}
		
		// input the players' name
		g.setFont(fontPlayerName);
		for (int i=0; i < players.size(); i++) {
			g.setColor(colorPlayersHUD[i]);
			
			// draws the player's name
			g.drawString(players.get(i).getName(),
				     	150,
				     	250 + (80 * i));
		}   	

		if(roundScores == null){
			int a = 0;
			System.out.println(a);
		}
		
		// draws the players' score
		g.setFont(fontPlayerScore);

		for (int i=0; i < game.getCurrentRound()+1; i++) {
			for (int k=0; k< players.size(); k++) {
				g.setColor(colorPlayersHUD[k]);
				g.drawString(Integer.toString(roundScores.get(i)[k]),
					((Dimensions.GAME_WIDTH - 200)/5) + 210 + (i*130),
			     	250 + (80 * k));
			}
		}
		
		if (!gameEnded) {
			List<Integer> winnerForRound = game.getWinnerForRound(game.getCurrentRound() + 1);
			
			for (int i=0; i<winnerForRound.size(); i++) {
				g.setFont(fontPlayerName);
				g.setColor(Color.WHITE);
				g.drawString("Winner for this Round: " + players.get(i).getName(),
							Dimensions.GAME_WIDTH/2 - 300,
							50 + (i*50));
			}
		}
		else {
			List<Integer> ultimateWinner = game.getUltimateWinner();
			
			for (int i=0; i<ultimateWinner.size(); i++) {
				g.setFont(fontPlayerName);
				g.setColor(Color.WHITE);
				g.drawString("ULTIMATE WINNER: " + players.get(i).getName(),
							Dimensions.GAME_WIDTH/2 - 300,
							50 + (i*50));
			}
		}
    }
    
    /**
     * This method renders the Current Time Left and the Current Round
     * in the Game Panel
     */
    public void drawGameInfo(Graphics g) {

		g.setFont(new Font("Courier New", Font.BOLD, 20));
    	
    	// gets the remaining time left in the game in the TronGame.java
		int timeLeft = ((GameManager) Client.getGameManager()).getGame().getCurrentTimeForRound();
		
		// gets the total rounds set in the game in the TronGame.java
		int durationPerRound = ((GameManager) Client.getGameManager()).getGame().getDurationPerRound();
		
    	
    	g.setColor(colorBlueTron);
    	g.drawString("TIME LEFT: " + (durationPerRound - timeLeft),
    				 5,
    				 20);
    	
    	int roundsLeft = ((GameManager) Client.getGameManager()).getGame().getCurrentRound();
    	
    	g.drawString("ROUND    : " + (roundsLeft + 1),
    				 5,
    				 40);

    }
    
	@Override
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		setupAntiAliasing(g2);
		
		drawBikes(g, g2);
		drawHUD(g);
		drawGrid(g);
		drawGameInfo(g);
		
		if (gameShowSummary)
			drawGameSummary(g);
	}
	
	public static void setGameShowSummary(boolean value) {
		gameShowSummary = value;
	}

	public static void setGameEnd() {
		gameEnded = true;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		this.requestFocusInWindow();
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
}
