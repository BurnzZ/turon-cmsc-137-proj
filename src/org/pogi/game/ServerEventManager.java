package org.pogi.game;


import java.net.InetSocketAddress;
import java.util.List;

import org.pogi.engine.net.EventManager;
import org.pogi.engine.net.IEvent;
import org.pogi.engine.net.Message;
import org.pogi.engine.net.constant.EventType;
import org.pogi.engine.net.javanet.Client;
import org.pogi.engine.net.javanet.Server;
import org.pogi.game.entity.Bike;
import org.pogi.game.entity.Player;

/**
 * Implementation for EventManager for the server side
 * @see EventManager
 */
public class ServerEventManager extends EventManager {
	@Override
	public void defineEvents() {
		//===========================		
		// GAMEPLAY EVENTS
		//===========================
		
		/*
		 * change direction of the bike of the player with the player name specified in the message
		 */
		putEventHandler(EventType.CHANGE_DIRECTION, new IEvent() {
			@Override
			public void apply(Message message) {
				//System.out.println("change direction");
				
				GameManager gm = (GameManager) Server.getGameManager();
				
				String username = message.getString("username");
				
				List<Player> players = gm.getGame().getPlayers();
				
				Player player = null;
				for(Player p : players){
					if(p.getName().equals(username)){
						player = p;
						break;
					}
				}
				
				List<Bike> bikes = gm.getGame().getBikes();
				Bike bike = null;
				for(Bike b : bikes){
					if(player.getBoardId() == b.getBoardId()){
						bike = b;
						break;
					}
				}
					
				bike.setDirection(message.get("direction").toString());
				
				
			}
		});
		
		//===========================		
		// GAME INITIALIZATION EVENTS
		//===========================
		
		/*
		 * initialize and start the game
		 */
		putEventHandler(EventType.GAME_START, new IEvent() {
			@Override
			public void apply(Message message) {
				//System.out.println("game start");
				
				GameManager gm = (GameManager) Server.getGameManager();
				gm.initRound();
				gm.startGame();
			}
		});
		
		//===========================		
		// OTHER EVENTS
		//===========================
		
		putEventHandler(EventType.PLAYER_DISCONNECTION, new IEvent() {
			@Override
			public void apply(Message message) {
				String username = message.getString("username");
				
				GameManager gm = (GameManager) Server.getGameManager();
				
				List<Player> players = gm.getGame().getPlayers();
				
				Player player = null;
				for(Player p : players){
					if(p.getName().equals(username)){
						player = p;
						break;
					}
				}
				
				gm.disconnectPlayer(player);
				Server.disconnectClient(player.getName());
				
			}
		});
		
		/*
		 * broadcast the chat sent by a client
		 */
		putEventHandler(EventType.CHAT, new IEvent() {
			@Override
			public void apply(Message message) {
				String chatMessage = message.getString("message");
				String username = message.getString("username");										
				
				Message messageToBroadcast = new Message(EventType.CHAT);
				messageToBroadcast
					.put("username", username)
					.put("message", chatMessage);
				
				Server.broadcastMessage(messageToBroadcast);
			}
		});
		
		/*
		 * initiate a handhake for first-time connecting clients and lets the client know
		 */
		putEventHandler(EventType.HANDSHAKE, new IEvent() {
			@Override
			public void apply(Message message) {
				//System.out.println("("+getClass()+") LOG: Applying Handshake Message...");
				
				InetSocketAddress clientSocketAddress = (InetSocketAddress) message.get("clientSocketAddress");
				String username = message.getString("username");
				Server.putClient(username, clientSocketAddress);
				
				GameManager gm = (GameManager) Server.getGameManager();

				if(gm.getGame().isOnGoing()){
					Message gameStartMessage = new Message(EventType.GAME_START);
					Server.broadcastMessage(gameStartMessage);
				}
				
				gm.newPlayer(username);

			}
		});
	}

}
