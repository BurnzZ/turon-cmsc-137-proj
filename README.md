-- CMSC 137 Final Project

Notes:
* wag kalimutang mag BRANCH
* yung mga COMMIT ay as verbose as possible

Chat Module Branch v0.5

Basics:

1. Run an Instance of GameServer
2. Run 1 or more Instance of GameGUI <-- GUI for the main client

Interface : 

GameClient
	getUsername, setUsername,
	getHostname, setHostname
	
ChatClient
	sendMessageToAll("message")	// call this onClick of a button or something. ie. [ Send ]
	onMessageReceived // Implement this. eg. Show message to text box

Commit logs
v0.5
- Initial Chat Module Branch
- simple string messages only - of format : "<TIMESTAMP> username:message"
- No Functional Chat GUI yet, only asks for username and hostname
2 important methods:
- sendMessageToAll(String message) 
- onMessageReceived(String message)
Missing features:
- Private Messaging
- Functional Chat GUI
TODO:
- refactoring
- use game framework

